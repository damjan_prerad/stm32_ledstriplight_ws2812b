

#include "WS2812.h"

StripChannels sc;

int main()
{
	//initWS2812Driver(&sc, GPIOH, 2, 0, 24, 4);
	initDefaultWS2812Driver(&sc, GPIOH);
	
	if(initRunningChannel(&sc,
										 0,//Pin 6 of the GPIOH port
										 24,//Number of LEDs
										 0xffff - 1,//Fastest is 0xffff, slowest is 1... 1 too sloow, speeds starting from 0xffff - 100 until 0xffff are more sensible
										 10, 0, 0,//Colors R, G, B
										 0, 30, 0,//Colors R, G, B
										 5) != E_OK)//Running width
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 1,
										 5,
										 0xffff - 100,
										 0, 0, 127,
										 127, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 2,
										 48,
										 0xffff - 2,
										 100, 0, 0,
										 0, 30, 0,
										 5) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 3,
										 30,
										 0xffff - 30,
										 0, 0, 127,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 4,
										 25,
										 0xffff - 3,
										 10, 0, 0,
										 0, 30, 0,
										 5) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 5,
										 15,
										 0xffff - 60,
										 0, 0, 127,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 6,
										 35,
										 0xffff - 4,
										 10, 0, 0,
										 0, 30, 0,
										 5) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 7,
										 40,
										 0xffff - 150,
										 0, 0, 127,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 8,
										 48,
										 0xffff - 10,
										 255, 0, 0,
										 0, 30, 0,
										 5) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 9,
										 1,
										 0xffff - 100,
										 0, 0, 255,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	startWS2812Driver();
	
	while(1)
	{
		//Do something else
		__nop();
		
	}
}
