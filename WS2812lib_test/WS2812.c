
/*
* Author: Damjan Prerad
* Language: C
* Date: 15/9/2020
*
* WS2832 LED strip driver. 
*
*/

#include "WS2812.h"

#ifdef DEFINES
#define RCC_PLLCFGR_PLLP_Pos       (16U)
#define RCC_PLLCFGR_PLLM_Pos       (0U)
#define RCC_PLLCFGR_PLLN_Pos       (6U)
#define RCC_PLLCFGR_PLLQ_Pos       (24U)
#define RCC_PLLCFGR_PLLSRC_Pos		 (22U)

#define DMA_SxCR_CHSEL_Pos (25U)
#define DMA_SxCR_DIR_Pos (6U)
#define DMA_SxCR_MSIZE_Pos (13U)
#define DMA_SxCR_PSIZE_Pos (11U)
#define DMA_SxCR_MBURST_Pos (23U)
#define DMA_SxCR_PBURST_Pos (21U)
#endif

#if !defined(RCC_PLLCFGR_PLLP_Pos) || !defined(RCC_PLLCFGR_PLLM_Pos) || !defined(RCC_PLLCFGR_PLLN_Pos) || !defined(RCC_PLLCFGR_PLLQ_Pos) || !defined(RCC_PLLCFGR_PLLSRC_Pos) ||\
!defined(DMA_SxCR_CHSEL_Pos) || !defined(DMA_SxCR_DIR_Pos) || !defined(DMA_SxCR_MSIZE_Pos) || !defined(DMA_SxCR_PSIZE_Pos) || !defined(DMA_SxCR_MBURST_Pos) || !defined(DMA_SxCR_PBURST_Pos)
#error PLEASE DEFINE -DEFINES- DIRECTIVE IN ORDER TO CONTINUE.
#endif

uint16_t BEGIN_HIGH[] = {0xffff};//Private variable, DMA, on TIM1 UP event
uint16_t BEGIN_LOW[] = {0x0000};//Private variable, dma, on TIM1 CCR2 event

StripChannels* internal = (void*)0;//When init is called this pointer is set
GPIO_TypeDef* _gpio_port;

/**
* Set GPIO pin as output to a corresponding channel output
*
* @param channelNumber : channel number (pin) to initialize
*
*/
void initChannel(uint8_t channelNumber)
{
	_gpio_port->MODER &= ~(0x01 << (channelNumber * 2));
	_gpio_port->MODER |= 0x01 << (channelNumber * 2);
	_gpio_port->OTYPER &= ~(0x00 << channelNumber);
	_gpio_port->OTYPER |= 0x00 << channelNumber;
	_gpio_port->OSPEEDR &= ~(0x00 << (channelNumber * 2));
	_gpio_port->OSPEEDR |= (0x11 << (channelNumber * 2));
	_gpio_port->PUPDR &= ~(0x00 << (channelNumber * 2));
	_gpio_port->PUPDR |= (0x00 << (channelNumber * 2));
}

/**
* Initialize a cahnnel with a running effect
*
* @param strips : structure used for storing channel initialization parameters
* @param channelNumber : channel number (pin) to initialize
* @param ledNum : number of leds on a given channel
* @param speed : speed of the effect (fastest is 0xffff, slowest is 0
* @param r1, g1, b1 : red, green, blue 8 bit values for the first color
* @param r2, g2, b2 : red, green, blue 8 bit values for the second color
* @param width : number of leds running at the same time (width of a rolling window)
*/
ErrorType initRunningChannel(StripChannels* strips, 
												uint8_t channelNumber, 
												uint16_t ledNum,
												uint16_t speed, 
												uint8_t r1, uint8_t g1, uint8_t b1,
												uint8_t r2, uint8_t g2, uint8_t b2,
												uint8_t width)
{
	if(channelNumber > 16) return E_INVALID_CHANNEL_ID;
	
	strips->channels[channelNumber].ledNum = ledNum;
	strips->channels[channelNumber].speed = 0xffff - speed;
	
	strips->channels[channelNumber].r1 = r1;
	strips->channels[channelNumber].g1 = g1;
	strips->channels[channelNumber].b1 = b1;
	
	strips->channels[channelNumber].r2 = r2;
	strips->channels[channelNumber].g2 = g2;
	strips->channels[channelNumber].b2 = b2;
	
	strips->channels[channelNumber].width = width;
	
	strips->channels[channelNumber].effect = RUNNING;
	
	initChannel(channelNumber);
	return E_OK;
}

/**
* Initialize a cahnnel with a flashing effect
*
* @param strips : structure used for storing channel initialization parameters
* @param channelNumber : channel number (pin) to initialize
* @param ledNum : number of leds on a given channel
* @param speed : speed of the effect (fastest is 0xffff, slowest is 0
* @param r1, g1, b1 : red, green, blue 8 bit values for the first color
* @param r2, g2, b2 : red, green, blue 8 bit values for the second color
*/
ErrorType initFlashingColorChannel(StripChannels* strips, 
												uint8_t channelNumber, 
												uint16_t ledNum,
												uint16_t speed, 
												uint8_t r1, uint8_t g1, uint8_t b1,
												uint8_t r2, uint8_t g2, uint8_t b2)
{
	if(channelNumber > 16) return E_INVALID_CHANNEL_ID;
	
	
	strips->channels[channelNumber].ledNum = ledNum;
	strips->channels[channelNumber].speed = 0xffff - speed;
	
	strips->channels[channelNumber].r1 = r1;
	strips->channels[channelNumber].g1 = g1;
	strips->channels[channelNumber].b1 = b1;
	
	strips->channels[channelNumber].r2 = r2;
	strips->channels[channelNumber].g2 = g2;
	strips->channels[channelNumber].b2 = b2;
	
	//strips->channels[channelNumber].width = width;
	
	strips->channels[channelNumber].effect = FLASHING_COLOR;
	
	strips->channels[channelNumber].pos = 0;
	
	initChannel(channelNumber);
	return E_OK;
}

/**
* 
*
* @param strips : structure used for storing channel parameters
* @param r, g, b : red, green, blue 8 bit values for the color
* @param channel : set pixel on a given channel
* @param position : set a pixel at given position
*/
void setColorOnChannelInPosition(StripChannels* strips,
																 uint8_t r, uint8_t g, uint8_t b,
																 uint8_t channel,
																 uint16_t position)
{
	uint16_t channelSelector = 0x01 << channel;
	for(int i = 0; i < 8; i++)
	{
		uint16_t bit_pos = 128 >> i;
		if(bit_pos & r)
		{
			strips->data[position].r[i] |= (channelSelector);
		}
		else
		{	
			strips->data[position].r[i] &= ~(channelSelector);
		}
		if(bit_pos & g)
		{		
			strips->data[position].g[i] |= (channelSelector);  
		}
		else
		{		
			strips->data[position].g[i] &= ~(channelSelector);
		}
		if(bit_pos & b)
		{
			strips->data[position].b[i] |= (channelSelector); 
		}
		else
		{
			strips->data[position].b[i] &= ~(channelSelector);
		}	
	}
}

/**
* Update a channel according to the preset effect
*
* @param strips : structure used for storing channel parameters
* @param channel : number of channel to be updated
*/
void updateChannel(StripChannels* strips,
									 uint8_t channelNumber)
{
	strips->channels[channelNumber].counter = ( strips->channels[channelNumber].counter + 1 ) % strips->channels[channelNumber].speed; 
	
	if(strips->channels[channelNumber].counter == 0)
	{
		switch(strips->channels[channelNumber].effect)
		{
			case FLASHING_COLOR:
					if(strips->channels[channelNumber].pos == 1)
					{
						strips->channels[channelNumber].pos = 0;
						for(int i = 0; i < strips->channels[channelNumber].ledNum; i++)
						{
							setColorOnChannelInPosition(strips,
																				strips->channels[channelNumber].r1, 
																				strips->channels[channelNumber].g1,
																				strips->channels[channelNumber].b1,
																				channelNumber,
																				i);
						}
					}
					else
					{
						strips->channels[channelNumber].pos = 1;
						for(int i = 0; i < strips->channels[channelNumber].ledNum; i++)
						{	
							setColorOnChannelInPosition(strips,
																				strips->channels[channelNumber].r2, 
																				strips->channels[channelNumber].g2,
																				strips->channels[channelNumber].b2,
																				channelNumber,
																				i);
						}
					}
			break;
			
			case RUNNING:
				for(int i = 0; i < strips->channels[channelNumber].ledNum; i++)
				{
					if(i >= strips->channels[channelNumber].pos && i <= (strips->channels[channelNumber].width + strips->channels[channelNumber].pos))
					{
						setColorOnChannelInPosition(strips,
																				strips->channels[channelNumber].r1, 
																				strips->channels[channelNumber].g1,
																				strips->channels[channelNumber].b1,
																				channelNumber,
																				i);
					}
					else
					{
						setColorOnChannelInPosition(strips,
																				strips->channels[channelNumber].r2, 
																				strips->channels[channelNumber].g2,
																				strips->channels[channelNumber].b2,
																				channelNumber,
																				i);
					}
				}
				strips->channels[channelNumber].pos = (strips->channels[channelNumber].pos + 1) % strips->channels[channelNumber].ledNum;
			break;
		}
	}
}

/**
* Updates all channels according to their preset effect
*
* @param strips : structure used for storing channel parameters
*/
void updateStripChannels(StripChannels* strips)
{
		for(int i = 0; i < 16; i++)
		{
			if(strips->channels[i].effect != 0)
			{
				updateChannel(strips, i);
			}
		}
}

/**
* Changes te internal StripChannels pointer to point to another sturcture
*
* @param strips : pointer to StripChannels structure where the parameters are initialized
*/
void changeActiveStripChannels(StripChannels* strips)
{
	internal = strips;
}


void initPLL(uint8_t PLLQ, uint8_t PLLP, uint8_t PLLN, uint8_t PLLM)
{
	FLASH->ACR &= ~(0x00000107);//Clear prefetch and latency
	FLASH->ACR |= (FLASH_ACR_LATENCY | FLASH_ACR_PRFTEN);//Set latency to 1 and enable prefetch
	
	//RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLSRC);
	RCC->PLLCFGR &= 0;
	RCC->PLLCFGR |= (PLLQ << RCC_PLLCFGR_PLLQ_Pos) | (0 << RCC_PLLCFGR_PLLSRC_Pos) | (PLLP << RCC_PLLCFGR_PLLP_Pos) | (PLLN << RCC_PLLCFGR_PLLN_Pos) | (PLLM << RCC_PLLCFGR_PLLM_Pos);
	//0x02000fc2
	//0b0000 0010 0000 0000 0000 1111 1100 0010
	//0b0000 0010 0000 0010 0000 0110 0000 0100
	RCC->CR |= (RCC_CR_PLLON);
	while(!(RCC->CR & RCC_CR_PLLRDY)){}
	RCC->CFGR &= ~(RCC_CFGR_SW);
	RCC->CFGR |= (RCC_CFGR_SW_PLL);
	while(!(RCC->CFGR & RCC_CFGR_SWS_PLL)){}
}

void initGPIOport()//CHANGE
{
	switch((int)_gpio_port)
	{
		case (int)GPIOA:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOAEN);
		break;
		case (int)GPIOB:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOBEN);
		break;
		case (int)GPIOC:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOCEN);
		break;
		case (int)GPIOD:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIODEN);
		break;
		case (int)GPIOE:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOEEN);
		break;
		case (int)GPIOF:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOFEN);
		break;
		case (int)GPIOG:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOGEN);
		break;
		case (int)GPIOH:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOHEN);
		break;
		case (int)GPIOI:
			RCC->AHB1ENR |= (RCC_AHB1ENR_GPIOIEN);
		break;
	}
}

void initTIM1(uint32_t period, uint32_t ccr1_val, uint32_t ccr2_val)
{
	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;
	TIM1->CR1 |= TIM_CR1_ARPE;
	TIM1->ARR = period;//period 1.25uS - 156
	TIM1->PSC = 1;
	TIM1->EGR = TIM_EGR_UG;
	TIM1->DIER |= TIM_DIER_UDE;
	
	TIM1->DIER |= TIM_DIER_CC1DE | TIM_DIER_CC2DE;//Capture/Compare DMA requeste enable
	TIM1->CCR1 |= ccr1_val;//Duty cycle on channel 1 ... 400nS high time - 52
	TIM1->CCR2 |= ccr2_val;//Duty cycle on channel 2 .... 800nS high time - 104
	TIM1->CCER |= TIM_CCER_CC1E | TIM_CCER_CC2E;//ENABLE channel 1 and channel 2
	
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	NVIC_SetPriority(TIM2_IRQn, 0x03);
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->CR1 &= ~(TIM_CR1_CEN);
	RCC->APB1RSTR |=  (RCC_APB1RSTR_TIM2RST);
	RCC->APB1RSTR &= ~(RCC_APB1RSTR_TIM2RST);
	TIM2->PSC   = 1000;
  TIM2->ARR   = 1000;//down time (length of a reset pulse)
	TIM2->EGR  |= TIM_EGR_UG;
	TIM2->DIER |= TIM_DIER_UIE;
}

void initDMA()
{
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	DMA2_Stream5->CR |= (6 << DMA_SxCR_CHSEL_Pos) | (0x01 << DMA_SxCR_DIR_Pos) | (0x01 << DMA_SxCR_MSIZE_Pos) | (0x01 << DMA_SxCR_PSIZE_Pos) | DMA_SxCR_CIRC | DMA_SxCR_MINC ;
	DMA2_Stream5->M0AR  = (uint32_t)&BEGIN_LOW;
  DMA2_Stream5->PAR   = (uint32_t)&(_gpio_port->ODR);
  DMA2_Stream5->NDTR  = (uint16_t)1;
  DMA2_Stream5->CR |= (DMA_SxCR_EN);
	
	DMA2_Stream2->CR |= (6 << DMA_SxCR_CHSEL_Pos) 
										| (0x01 << DMA_SxCR_DIR_Pos) 
										| (0x01 << DMA_SxCR_MSIZE_Pos) 
										| (0x01 << DMA_SxCR_PSIZE_Pos) 
										| (0x00 << DMA_SxCR_MBURST_Pos) 
										| (0x00 << DMA_SxCR_PBURST_Pos) 
										| DMA_SxCR_MINC ;//THIS SHOULD NOT BE CIRC
	DMA2_Stream2->M0AR  = (uint32_t)&(internal->data);//&(internal->data);
  DMA2_Stream2->PAR   = (uint32_t)&(_gpio_port->ODR);
  DMA2_Stream2->NDTR  = (uint16_t)MAX_LED_NUMBER * 24;
	DMA2_Stream2->CR |= (DMA_SxCR_TCIE);
  DMA2_Stream2->CR |= (DMA_SxCR_EN);
	NVIC_SetPriority(DMA2_Stream2_IRQn, 0);
	NVIC_EnableIRQ(DMA2_Stream2_IRQn);
	
	DMA2_Stream1->CR |= (6 << DMA_SxCR_CHSEL_Pos) | (0x01 << DMA_SxCR_DIR_Pos) | (0x01 << DMA_SxCR_MSIZE_Pos) | (0x01 << DMA_SxCR_PSIZE_Pos) | DMA_SxCR_CIRC | DMA_SxCR_MINC ;
	DMA2_Stream1->M0AR  = (uint32_t)&BEGIN_HIGH;
  DMA2_Stream1->PAR   = (uint32_t)&(_gpio_port->ODR);
  DMA2_Stream1->NDTR  = (uint16_t)1;
  DMA2_Stream1->CR |= (DMA_SxCR_EN);
}

void startWS2812Driver(void)
{
	TIM1->CR1 |= TIM_CR1_CEN;
}

void stopWS2812Driver(void)
{
	TIM1->CR1 &= ~(TIM_CR1_CEN);
}

/*
*	Initialize required peripherals. Clock source of 16MHz is assumed
* PLL is selected as clock source
* frequency is set at 48MHz
*
*	@param sc : structure used for storing channel parameters
*	@param gpio_port : output port for the WS2812 driver
*/
void initDefaultWS2812Driver(StripChannels* sc, GPIO_TypeDef* gpio_port)
{
	
	_gpio_port = gpio_port;
	internal = sc;
	
	initPLL(2, 0, 24, 4);
	initGPIOport();
	initTIM1(60, 19, 38);//156, 52, 104
	initDMA();
}

/*
*	Initialize required peripherals. Clock source of 16MHz is assumed
* PLL is selected as clock source
* frequency is calculated according to the formula f = ((16000000 / PLLM) * PLLN) / PLLP
*
*	@param sc : structure used for storing channel parameters
*	@param gpio_port : output port for the WS2812 driver
*	@param PLLQ : PLL clock config Q division factor (USB OTG FS, SDIO and Random Number Generator)
*	@param PLLP : PLL clock config P main division factor
*	@param PLLN : PLL clock config N multiplication factor
*	@param PLLM : PLL clock config M division factor
*/
void initWS2812Driver(StripChannels* sc, GPIO_TypeDef* gpio_port, uint8_t PLLQ, uint8_t PLLP, uint8_t PLLN, uint8_t PLLM)//2, 0, 24, 4
{
	_gpio_port = gpio_port;
	internal = sc;
	
	uint32_t freq = (16 * PLLN) / ((PLLP + 1) * 2) / PLLM;//48
	
	uint32_t period = (uint32_t)(freq * 125) / 100;//multiplying by 2 gives right results... somewhere the prescaler by 2 is activated.... possibly AHB or APB peripheral prescaler
	uint32_t low = (uint32_t)(freq * 4) / 10;
	uint32_t high = (uint32_t)(freq * 8) / 10;
	
	initPLL(PLLQ, PLLP, PLLN, PLLM);//2, 0, 24, 4
	initGPIOport();
	initTIM1(period, low, high);//156, 52, 104
	initDMA();
}

void DMA2_Stream2_IRQHandler(void)
{
		if (DMA2->LISR & DMA_LISR_TCIF2) {
			// clear stream 1 transfer complete interrupt
			DMA2->LIFCR |= (DMA_LIFCR_CTCIF2);
			_gpio_port->ODR = 0x0000;//Reset port
    
			TIM1->CCER &= ~(TIM_CCER_CC1E | TIM_CCER_CC2E);
			TIM1->CR1 &= ~(TIM_CR1_CEN);
			//_gpio_port->ODR = 0x0000;//Reset port
			updateStripChannels(internal);
			
			TIM2->CR1  |= TIM_CR1_CEN;
    }
}

void TIM2_IRQHandler(void)
{
	if(TIM2->SR & TIM_SR_UIF)
	{
		TIM2->SR &= ~(TIM_SR_UIF);
		TIM2->CR1 &= ~(TIM_CR1_CEN);
		DMA2_Stream2->CR |= (DMA_SxCR_EN);
		TIM1->CCER |= (TIM_CCER_CC1E | TIM_CCER_CC2E);
		TIM1->CR1 |= TIM_CR1_CEN;
	}
}
