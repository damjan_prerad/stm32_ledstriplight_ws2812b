
#ifndef WS2812_H
#define WS2812_H

#include <stdint.h>
#include "stm32f4xx.h"

//#define DEFINES//Used for definitions requierd for PLL and DMA setup

#define MAX_LED_NUMBER 48 //LED strip with density of 1 LED / cm of maximum length 2.5 m. For better performance reduce this number


typedef enum _ErrorType
{
	E_OK = 0,
	E_INVALID_CHANNEL_ID = 1,
	E_INVALID_EFFECT = 2//Currently there is no use for this error
} ErrorType;

typedef struct __attribute__((packed)) _Pixel
{
	uint16_t g[8];
	uint16_t r[8];
	uint16_t b[8];
} Pixel;

typedef enum _Effect
{
	RUNNING = 1,
	FLASHING_COLOR = 2
} Effect;

typedef struct _Channel
{
	uint16_t ledNum;
	uint16_t speed;
	uint16_t counter;
	//Color1
	uint8_t r1;
	uint8_t g1;
	uint8_t b1;
	//Color2
	uint8_t r2;
	uint8_t g2;
	uint8_t b2;
	//RUNNING effect variables
	Effect effect;
	uint8_t width;
	uint8_t pos;
} Channel;

typedef struct _StripChannels
{
	Channel channels[16];
	
	Pixel data[MAX_LED_NUMBER];
} StripChannels;


ErrorType initRunningChannel(StripChannels* strips, 
												uint8_t channelNumber, 
												uint16_t ledNum,
												uint16_t speed, 
												uint8_t r1, uint8_t g1, uint8_t b1,
												uint8_t r2, uint8_t g2, uint8_t b2,
												uint8_t width);

ErrorType initFlashingColorChannel(StripChannels* strips, 
												uint8_t channelNumber, 
												uint16_t ledNum,
												uint16_t speed, 
												uint8_t r1, uint8_t g1, uint8_t b1,
												uint8_t r2, uint8_t g2, uint8_t b2);

void initWS2812Driver(StripChannels* sc, GPIO_TypeDef* gpio_port, uint8_t PLLQ, uint8_t PLLP, uint8_t PLLN, uint8_t PLLM);
void initDefaultWS2812Driver(StripChannels* sc, GPIO_TypeDef* gpio_port);
	
void startWS2812Driver(void);
void stopWS2812Driver(void);

#endif //WS2812_H
