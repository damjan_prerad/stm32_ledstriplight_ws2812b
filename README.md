# STM32_LEDstripLight_WS2812B

Na ovoj grani se nalazi gotova WS2812 biblioteka.
Od interesa su dva fajla: WS2812.c i WS2812.h

U folderu WS28121lib_test je primjer projekat.

Biblioteka koristi DMA prenos podataka iz buffera na odabrani GPIO port. Posto je GPIO port sirine 16 bita, onda i biblioteka moze da upravlja sa 16 nezavisnih LED traka.

Implementirana su dva efekta:
- RUNNING
- FLASH_COLOR

Jednom kada se sve inicijalizuje, nije potrebno nista vise raditi u glavnoj petlji programa. Sve potrebne funkcije se obavljaju u TIM1 prekidnoj rutini.