/**************************************************************************************************
 * Copyright (c) 2020 Innovation system. All rights reserved. Confidential proprietary
 * Nenada Kostica 129, Banja Luka, Bosnia and Herzegovina. innovation.systems.plus@gmail.com
 *************************************************************************************************/
/**********************************************************************************************//**
 * \file USER_TYPE.h
 *
 * \latexonly \hypertarget{USER_TYPE}{} \endlatexonly
 *
 * \brief Global user types
 *
 *  This header file defines user types for the driver.
 *
 *************************************************************************************************/

#ifndef USER_TYPE_H_
#define USER_TYPE_H_

/**************************************************************************************************
 *
 * D E F I N I T I O N S
 *
 *************************************************************************************************/

/************************************** Common Definitions ***************************************/

/** @def NULL_PTR
*   @brief NULL pointer definition
*/
#ifndef NULL_PTR
    #define NULL_PTR            ((void *)0U)
#endif

#ifndef bool
    typedef unsigned char       bool;   /**< @brief boolean type */
#endif


/* for compatibility reasons only */
/** @def FALSE
*   @brief bool definition for FALSE (deprecated, please use #TT_FALSE instead)
*/
#ifndef FALSE
    #define FALSE               ((bool)0U)
#endif

/** @def TRUE
*   @brief bool definition for TRUE (deprecated, please use #TT_TRUE instead)
*/
#ifndef TRUE
    #define TRUE                ((bool)1U)
#endif

#endif /* USER_TYPE_H_ */
