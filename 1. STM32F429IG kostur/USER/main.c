/**
 *	Keil project template
 *
 *	Before you start, select your target, on the right of the "Load" button
 *
 *	@author		Dragan Nedimovic
 *	@email		tilen@majerle.eu
 *	@website	http://stm32f4-discovery.com
 *	@ide		Keil uVision 5
 *	@conf		PLL parameters are set in "Options for Target" -> "C/C++" -> "Defines"
 *	@packs		STM32F4xx Keil packs version 2.2.0 or greater required
 *	@stdperiph	STM32F4xx Standard peripheral drivers version 1.4.0 or greater required
 */

/* Include core modules */
#include "stm32f4xx.h"

/* In stdio.h file is everything related to output stream */
#include "stdio.h"
#include "config.h"

#include "WS2812.h"

StripChannels sc;

/* Private functions ---------------------------------------------------------*/



/*******************************************************************************
* Function Name  : main
* Description    : Main Programme
* Input          : None
* Output         : None
* Return         : None
* Attention      : None
*******************************************************************************/
int main(void) {
	
	
	//initWS2812Driver(&sc, GPIOH, 2, 0, 24, 4);
	initDefaultWS2812Driver(&sc, GPIOH);
	
	if(initRunningChannel(&sc,
										 0,//Pin 6 of the GPIOH port
										 48,//Number of LEDs
										 0xffff - 1,//Fastest is 0xffff, slowest is 1... 1 too sloow, speeds starting from 0xffff - 100 until 0xffff are more sensible
										 150, 0, 0,//Colors R, G, B
										 0, 150, 0,//Colors R, G, B
										 5) != E_OK)//Running width
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 1,
										 5,
										 0xffff - 100,
										 0, 0, 150,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 2,
										 48,
										 0xffff - 2,
										 150, 0, 150,//Colors R, G, B
										 0, 150, 50,//Colors R, G, B
										 7) != E_OK)//Running width
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 3,
										 10,
										 0xffff - 100,
										 0, 40, 150,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 4,
										 48,
										 0xffff - 3,
										 126, 20, 60,
										 45, 126, 10,
										 1) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 5,
										 15,
										 0xffff - 100,
										 20, 10, 150,
										 60, 60, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 6,
										 48,
										 0xffff - 4,
										 150, 0, 200,
										 10, 0, 0,
										 20) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 7,
										 20,
										 0xffff - 100,
										 0, 20, 150,
										 20, 10, 0) != E_OK)
	{
		return 0;
	}
	
	if(initRunningChannel(&sc,
										 8,
										 48,
										 0xffff - 5,
										 0x3c, 0, 0x04,
										 0x04, 0x3c, 0x04,
										 2) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 9,
										 30,
										 0xffff - 50,
										 20, 20, 20,
										 60, 60, 80) != E_OK)
	{
		return 0;
	}
	
	if(initFlashingColorChannel(&sc,
										 10,
										 48,
										 0xffff - 8,
										 0, 10, 150,
										 60, 20, 0) != E_OK)
	{
		return 0;
	}
	
	startWS2812Driver();
	
	while(1)
	{
		//Do something else
		__nop();
		
	}
	
}




