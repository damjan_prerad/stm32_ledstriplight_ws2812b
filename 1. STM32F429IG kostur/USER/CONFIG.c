/**
 *	Keil project template
 *
 *
 *	@author		Dragan Nedimovic
 *	@email		innovation.systems.plus@gmail.com
 *	@website	
 *	@ide		Keil uVision 5
 *	@conf		
 *	@packs		
 *	@stdperiph	
 */

/* Include core modules */
#include "CONFIG.h"
#include "stm32f4xx.h"

/*******************************************************************************
* Function Name  : GPIO Configuration
* Description    : 
* Input          : None
* Output         : None
* Return         : None
* Attention      : None
*******************************************************************************/
void GPIO_Configuration(void);

/*******************************************************************************
* Function Name  : TIM2_Configuration
* Description    : Timer 2 configuration
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TIM2_Configuration(void);

/*******************************************************************************
* Function Name  : NVIC_TIM2_Configuration
* Description    : Configuration the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_TIM2_Configuration(void);

/*******************************************************************************
* Function Name  : TIM3_Configuration
* Description    : Timer 3 configuration
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TIM3_Configuration(void);

/*******************************************************************************
* Function Name  : NVIC_TIM3_Configuration
* Description    : Configuration the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_TIM3_Configuration(void);

/*******************************************************************************
* Function Name  : GPIO Configuration
*******************************************************************************/
void CONFIG_GPIO_Configuration(void)
{
   GPIO_InitTypeDef GPIO_InitStructure;
	
		/* Enable GPIO AHB Clocks */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB |RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE | 
                         RCC_AHB1Periph_GPIOF |RCC_AHB1Periph_GPIOG | RCC_AHB1Periph_GPIOH | RCC_AHB1Periph_GPIOI , ENABLE);		

	
//	/* GPIOB Configuration: FMC pins - define like Digital OUTPUTS */ 
//	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1  ;
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
//	GPIO_Init(GPIOB, &GPIO_InitStructure);
//	
	
	/* GPIOC Configuration: FMC pins - define like Digital INPUTS KEY1*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/* GPIOC Configuration: FMC pins - define like Digital INPUTS KEY2*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_3; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
		//*************************************REEL4*******************************//	
	  /* GPIOH Configuration: LCD pins - define as Digital OUTPUT - STEP=PF10  REEL4 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOF, &GPIO_InitStructure);
	
		  /* GPIOH Configuration: LCD pins - define as Digital OUTPUT - DIR=PA3  REEL4 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
		  /* GPIOH Configuration: LCD pins - define as Digital OUTPUT - EN=PB10  REEL4 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
		/* GPIOH Configuration: LCD pins - define as Digital INPUTS - SIGNAL=PH6 REEL4*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_6; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
	
	//*************************************REEL3*******************************//	
		/* GPIOH Configuration: LCD pins - define as Digital OUTPUT - STEP=PH9 DIR=PH10 EN=PH12 REEL3 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
	
		/* GPIOH Configuration: LCD pins - define as Digital INPUTS - SIGNAL=PH11 REEL3*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_11; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
	
	//*************************************REEL2*******************************//
	/* GPIOD Configuration: FMC pins - define as Digital OUTPUT - STEP=PD4 DIR=PD5 REEL2 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_4 | GPIO_Pin_5 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	/* GPIOG Configuration: FMC pins - define as Digital OUTPUT - EN=PG10 REEL2 */ 
	GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOG, &GPIO_InitStructure);
	
	/* GPIOE Configuration: FMC pins - define as Digital INPUTS - SIGNAL=PE2 REEL2*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOE, &GPIO_InitStructure); 
	
		//*************************************REEL1*******************************//
	/* GPIOD Configuration: FMC pins - define as Digital OUTPUT - STEP=PD0 DIR=PD1, EN=PD14 REEL1 */ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 |  GPIO_Pin_14 ;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
		/* GPIOE Configuration: FMC pins - define as Digital INPUTS - SIGNAL=PD15 REEL1*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_15; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOD, &GPIO_InitStructure); 
		
		//*************************************REEL0*******************************//	
	/* GPIOE Configuration: FMC pins - define as Digital OUTPUTS - STEP=PE9, DIR=PE10, EN=PE7 REEL0 */ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7 | GPIO_Pin_9| GPIO_Pin_10; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure); 
	
		/* GPIOE Configuration: FMC pins - define as Digital INPUTS - SIGNAL=PE8 REEL0*/ 
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8; 
	GPIO_InitStructure.GPIO_Mode =  GPIO_Mode_IN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 	
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOE, &GPIO_InitStructure); 
	
	//***********************************************************************************//
//	/* GPIOG Configuration:  LCD pins - define as Digital OUTPUTS */ 
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_10 | GPIO_Pin_11 ; 
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;	
//	GPIO_Init(GPIOG, &GPIO_InitStructure);
//	
//	/* GPIOH Configuration:  LCD pins - define as Digital OUTPUTS */ 
//	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15; 
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
//	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
//	GPIO_Init(GPIOH, &GPIO_InitStructure);
//	

//*********RS485 TX and RX enable pins***************//
//	/* GPIOI Configuration:  Enable Tx, Rx - define as Digital OUTPUTS */ 
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_11; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz; 
	GPIO_Init(GPIOI, &GPIO_InitStructure);
}


/*******************************************************************************
* Function Name  : TIM2_Configuration
* Description    : This timer is used for STEPPER set position(STEPPER.c)
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TIM2_Configuration()
{
	//timer 2 config
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2 , ENABLE);
  TIM_DeInit(TIM2);
  TIM_TimeBaseStructure.TIM_Period = 810 - 1;	// 50uS period
  TIM_TimeBaseStructure.TIM_Prescaler = 0;				    
  TIM_TimeBaseStructure.TIM_ClockDivision = 0; 			
  TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
  TIM_ClearFlag(TIM2, TIM_FLAG_Update);							    
  TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
  TIM_Cmd(TIM2, ENABLE);		
}

/*******************************************************************************
* Function Name  : TIM3_Configuration
* Description    : This timer is used for STEPPER set position(STEPPER.c)
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TIM3_Configuration()
{
	//timer 3 config
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 , ENABLE);
  TIM_DeInit(TIM3);
  TIM_TimeBaseStructure.TIM_Period = 40500 - 1;	// 2.5mS period 	
  TIM_TimeBaseStructure.TIM_Prescaler = 0;				    
  TIM_TimeBaseStructure.TIM_ClockDivision = 0; 			
  TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
  TIM_ClearFlag(TIM3, TIM_FLAG_Update);							    
  TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
  TIM_Cmd(TIM3, ENABLE);		
}

/*******************************************************************************
* Function Name  : TIM4_Configuration
* Description    : This timer is used for MEI_SC8307 Uart send data
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void TIM4_Configuration()
{
	//timer 4 config
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4 , ENABLE);
  TIM_DeInit(TIM3);
  TIM_TimeBaseStructure.TIM_Period = 32768;	// oko 1s period 	
  TIM_TimeBaseStructure.TIM_Prescaler = 256;				    
  TIM_TimeBaseStructure.TIM_ClockDivision = 0; 			
  TIM_TimeBaseStructure.TIM_CounterMode=TIM_CounterMode_Up; 		
  TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);
  TIM_ClearFlag(TIM4, TIM_FLAG_Update);							    
  TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
  TIM_Cmd(TIM4, ENABLE);		
}

/*******************************************************************************
* Function Name  : NVIC_TIM2_Configuration
* Description    : Configuration the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_TIM2_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure; 
	 
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);  													
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;	  
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;	
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : NVIC_TIM3_Configuration
* Description    : Configuration the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_TIM3_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure; 
	 
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3);  													
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;	  
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;	
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : NVIC_TIM3_Configuration
* Description    : Configuration the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_TIM4_Configuration(void)
{
  NVIC_InitTypeDef NVIC_InitStructure; 
	 
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);  													
  NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;	  
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 4;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;	
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}


/*******************************************************************************
* Function Name  : USART1_Configuration
* Description    : Configure USART1 
* Input          : None
* Output         : None
* Return         : None
* Attention      : None
*******************************************************************************/
void USART1_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure; 
  NVIC_InitTypeDef NVIC_InitStructure;
	
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

  /* Configure GPIO to the USART1 */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

  /*
  *  USART1_TX -> PA9 , USART1_RX ->	PA10
  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_Even;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART1, &USART_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;  
  NVIC_Init(&NVIC_InitStructure);
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART1, ENABLE);
}

/*******************************************************************************
* Function Name  : USART6_Configuration //RS485
* Description    : Configure USART6 //RS485 
* Input          : None
* Output         : None
* Return         : None
* Attention      : None
*******************************************************************************/
void USART6_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  USART_InitTypeDef USART_InitStructure; 
  NVIC_InitTypeDef NVIC_InitStructure;
	
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, ENABLE);

  /* Configure GPIO to the USART6 */
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);

  /*
  *  USART6_TX -> PC6 , USART6_RX ->	PC7
  */
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOC, &GPIO_InitStructure);

  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

  USART_Init(USART6, &USART_InitStructure);
  NVIC_InitStructure.NVIC_IRQChannel = USART6_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0f;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;  
  NVIC_Init(&NVIC_InitStructure);
  USART_ITConfig(USART6, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART6, ENABLE);
}

/*******************************************************************************
* Function Name  : CONFIG_TIM_Init
* Description    : This function is used for timer configuration of all driver.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void CONFIG_TIM_Init(void)
{
	TIM2_Configuration();
	NVIC_TIM2_Configuration();
	TIM3_Configuration();
	NVIC_TIM3_Configuration();
	TIM4_Configuration();
	NVIC_TIM4_Configuration();
}
