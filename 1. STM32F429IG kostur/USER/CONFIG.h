/**************************************************************************************************
 * Copyright (c) 2020 Innovation system. All rights reserved. Confidential proprietary
 * Nenada Kostica 129, Banja Luka, Bosnia and Herzegovina. innovation.systems.plus@gmail.com
 *************************************************************************************************/
/**********************************************************************************************//**
 * \file CONFIG.h
 *
 * \latexonly \hypertarget{STEPPER}{} \endlatexonly
 *
 * @brief File containing function and typedef definitions for controling of a Slot Reel with steppr motor
 *
 * Driver used for control is Allegro MicroSystems A3977
 * It is initialy configured as 1/4 stepp driver but that can be modified with MS1 and MS0 pins.
 * This library uses TIM1 for pulse generation, so please avoid using TIM1 elswhere in your application
 * @see  https://www.digikey.com/en/datasheets/allegromicrosystemsllc/allegro-microsystems-llca3967datasheetashx
 

 */

#ifndef _CONFIG_H_
#define _CONFIG_H_


void CONFIG_TIM_Init(void);
void CONFIG_GPIO_Configuration(void);
void USART1_Configuration(void);
void USART6_Configuration(void);
#endif
