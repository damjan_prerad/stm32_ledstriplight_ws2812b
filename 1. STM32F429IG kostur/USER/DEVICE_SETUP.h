/**************************************************************************************************
 * Copyright (c) 2020 Innovation system. All rights reserved. Confidential proprietary
 * Nenada Kostica 129, Banja Luka, Bosnia and Herzegovina. innovation.systems.plus@gmail.com
 *************************************************************************************************/
/**********************************************************************//**
 * \file DEVICE_SETUP.h
 *
 * \latexonly \hypertarget{DEVICE_SETUP}{} \endlatexonly
 *
 * \brief General device setup
 *
 *  This header file defines general setup for device:
 *	*	number of device reels
 *	*	number of position per reels
 *
**************************************************************************/

#ifndef DEVICE_SETUP_H_
#define DEVICE_SETUP_H_

/**************************************************************************
 *
 * I N C L U D E S
 *
 **************************************************************************/
 
 #include "stm32f4xx.h"
 
/**************************************************************************
 *
 * D A T A   S T R U C T U R E
 *
 **************************************************************************/

/**************************************************************************
 *
 * D E F I N I I T O N S
 *
 **************************************************************************/

/*
 * D E V I C E   S E T U P  FOR R E E L S
 */
/**
 * \name Number of reels
 * 
 */
/** number of device reels. */
#define DEVICE_SETUP_NO_OF_REELS        		 ((uint8_t)0x05)  

/**
 * \name Number of positions per reels
 * 
 */
/** Number of position for reel 0. */
#define DEVICE_SETUP_NO_OF_POS_REEL_0        ((uint8_t)0x14)

/** Number of position for reel 0. */
#define DEVICE_SETUP_NO_OF_POS_REEL_1        ((uint8_t)0x14)

/** Number of position for reel 0. */
#define DEVICE_SETUP_NO_OF_POS_REEL_2        ((uint8_t)0x14)

/** Number of position for reel 0. */
#define DEVICE_SETUP_NO_OF_POS_REEL_3        ((uint8_t)0x14)

/** Number of position for reel 0. */
#define DEVICE_SETUP_NO_OF_POS_REEL_4        ((uint8_t)0x14)

/** Max number of position for reels. */
#define DEVICE_SETUP_MAX_NO_OF_POS_REEL        ((uint8_t)0x14)

/**
 * \name Reel symbols
 *
 * @{
 */
/** Reel symbol BLANK. */
#define DEVICE_SETUP_REEL_SYMBOL_BLANK      		 	 	 ((uint8_t)0x00) 

/** Reel symbol 7_GOLD. */
#define DEVICE_SETUP_REEL_SYMBOL_7_GOLD        		 	 ((uint8_t)0x01)  

/** Reel symbol 7_RED. */
#define DEVICE_SETUP_REEL_SYMBOL_7_RED        		 	 ((uint8_t)0x02) 

/** Reel symbol 7_GREEN. */
#define DEVICE_SETUP_REEL_SYMBOL_7_GREEN        	 	 ((uint8_t)0x03)

/** Reel symbol BARx3. */ 
#define DEVICE_SETUP_REEL_SYMBOL_BARx3        		 	 ((uint8_t)0x04) 

/** Reel symbol BARx2. */ 
#define DEVICE_SETUP_REEL_SYMBOL_BARx2        		 	 ((uint8_t)0x05) 

/** Reel symbol BARx1. */ 
#define DEVICE_SETUP_REEL_SYMBOL_BARx1        		 	 ((uint8_t)0x06) 

/** Reel symbol BARx1. */ 
#define DEVICE_SETUP_MAX_NO_OF_POS_REEL        		 ((uint8_t)0x14) 
/** @} */

const static uint8_t DEVICE_SETUP_NO_POS_REEL[DEVICE_SETUP_NO_OF_REELS] = 
{ 	DEVICE_SETUP_NO_OF_POS_REEL_0	/**< Number of positions for reel 0 */
	, DEVICE_SETUP_NO_OF_POS_REEL_1	/**< Number of positions for reel 1 */
	, DEVICE_SETUP_NO_OF_POS_REEL_2	/**< Number of positions for reel 2 */
	, DEVICE_SETUP_NO_OF_POS_REEL_3	/**< Number of positions for reel 3 */
	, DEVICE_SETUP_NO_OF_POS_REEL_4	/**< Number of positions for reel 4 */
};	

/** predetermined arrangement of symbol positions  */
const static uint8_t MATH_SETUP_ARRAY_REEL[DEVICE_SETUP_NO_OF_REELS][DEVICE_SETUP_MAX_NO_OF_POS_REEL] = 
{ 
		/** REEL 0  */
	{ DEVICE_SETUP_REEL_SYMBOL_7_GREEN	
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_7_RED
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GREEN
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GOLD
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	},
		/** REEL 1  */
	{ DEVICE_SETUP_REEL_SYMBOL_7_GREEN	
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_7_RED
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GREEN
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GOLD
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	},
		/** REEL 2  */
	{ DEVICE_SETUP_REEL_SYMBOL_7_GREEN	
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_7_RED
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GREEN
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GOLD
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	},
		/** REEL 3  */
	{ DEVICE_SETUP_REEL_SYMBOL_7_GREEN	
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_7_RED
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GREEN
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GOLD
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	},
		/** REEL 4  */
	{ DEVICE_SETUP_REEL_SYMBOL_7_GREEN	
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK	
	, DEVICE_SETUP_REEL_SYMBOL_7_RED
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GREEN
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_7_GOLD
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx1
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx3
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	, DEVICE_SETUP_REEL_SYMBOL_BARx2
	, DEVICE_SETUP_REEL_SYMBOL_BLANK
	},
};																				


#endif /* DEVICE_SETUP_H_ */
